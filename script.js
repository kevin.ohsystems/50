document.body.style.cursor = 'none'; // Hide the cursor

const counterEl = document.getElementById('counter');
let seconds = 0;
let blinkIntervalId;

function updateTimer() {
  seconds += 1;
  // ... rest of the updateTimer function ...

  if (minutes >= 45) {
    updateBlinkingEffect(minutes);
  }

  // ... rest of the updateTimer function ...
}

function applyGradientEffect() {
  // ... gradient effect code ...
}

let timer = setInterval(updateTimer, 1000);

document.addEventListener('keydown', function(e) {
  if (e.key === 'ArrowRight') {
    seconds += 60;
    let newMinutes = Math.floor(seconds / 60);
    updateBlinkingEffect(newMinutes);
    updateTimer();
  }
});

function updateBlinkingEffect(minutes) {
  // Calculate the blink rate
  let blinkRate = Math.max(60000 - ((minutes - 45) * 1150), 1000);

  if (blinkIntervalId) {
    clearInterval(blinkIntervalId);
    console.log('Cleared interval:', blinkIntervalId);
  }

  // Only start blinking if we're past minute 45
  if (minutes >= 45) {
    blinkIntervalId = setInterval(blinkBackground, blinkRate);
    console.log('Set interval:', blinkIntervalId, 'with rate:', blinkRate);
  }
}

function blinkBackground() {
  const colors = ['#ff0000', '#ffa500', '#ffff00', '#008000', '#0000ff', '#4b0082', '#ee82ee'];
  const randomColor = colors[Math.floor(Math.random() * colors.length)];
  document.body.style.backgroundColor = randomColor;
  console.log('Blink with color:', randomColor);
}
